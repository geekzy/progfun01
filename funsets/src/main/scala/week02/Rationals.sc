val x = new Rational(1, 3)
val y = new Rational(5, 7)
val z = new Rational(3, 2)
val a = new Rational(2)
type Set = Int => Boolean

x
x.add(y)
x.sub(y).sub(z)
x.max(y)
z.max(y)
a

class Rational(x: Int, y: Int) {
  require(y > 0, "denominator must be positive!")

  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)

  def this(x: Int) = this(x, 1)

  def numer = x

  def denom = y

  def add(that: Rational): Rational =
    new Rational(that.numer * denom + numer * that.denom, that.denom * denom)

  def neg: Rational = new Rational(-numer, denom)

  def sub(that: Rational): Rational = add(that.neg)

  def less(that: Rational): Boolean = numer * that.denom < that.numer * denom

  def max(that: Rational): Rational = if (less(that)) that else this

  override def toString = {
    val g = gcd(numer, denom)
    numer / g + "/" + denom / g
  }
}